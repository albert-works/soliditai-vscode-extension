  import * as vscode from 'vscode';
  const axios = require('axios');
  import * as fs from 'fs';
  import { Retrier } from '@jsier/retrier';
  import path = require('path');
  import { Readable } from 'stream';
  const unzip = require('unzip-stream');

  let soliditaiCurrentTestGenerationInProgress = '';

  export function activate(context: vscode.ExtensionContext) { 
    function fileIsSolidity(filename: string) {
      return filename.toLowerCase().endsWith('.sol');
    }

    function uploadAndPoll(httpConfig: any, filename: string, contents: string) {
      const baseFilename = path.parse(filename).base;
      vscode.window.showInformationMessage('Soliditai: Uploading ' + baseFilename);
      axios.post("https://soliditai.com/wp-json/file_uploader/v1/testable-script/", {
        "filename": baseFilename,
        "fromSource": "VS_CODE",
        "fileContents": contents
      }, httpConfig).then((response: any) => {
        if (soliditaiCurrentTestGenerationInProgress !== '') {
          vscode.window.showInformationMessage('Soliditai: File succesfully uploaded for test generation. Replaced previous polling.');
          soliditaiCurrentTestGenerationInProgress = response.data.chain_id;
        } else {
          vscode.window.showInformationMessage('Soliditai: File succesfully uploaded for test generation. Polling for result.');
          soliditaiCurrentTestGenerationInProgress = response.data.chain_id;
          pollResult(httpConfig, filename);
        }
      }, (error: any) => {
        console.log('Soliditai: ' + error);
        vscode.window.showErrorMessage(error.message);
      });

    }

    async function attemptCall(trie: number, httpConfig: any) {
      if (trie > 0) {
        vscode.window.showInformationMessage('Soliditai: Still polling for generated tests.');
      }
      return await axios.get("https://soliditai.com/wp-json/file_uploader/v1/test-script?chain_id=" + soliditaiCurrentTestGenerationInProgress, httpConfig);
    };

    function handleResult (result: any, filename: string) {
      if (result.status === "OK") {
        const projectPath = vscode.workspace.workspaceFolders?.map(folder => folder.uri.fsPath).filter(p => filename.startsWith(p))[0] ?? '';
        if (projectPath === '') {
          vscode.window.showErrorMessage("Failed to determine project path to place the generated tests. Visit https://soliditai.com/try-it-out/ to download your tests.");
          console.error("Failed to determine project path, because " + filename + " doesn't start with any of (see next error message):");
          console.error(vscode.workspace.workspaceFolders?.map(folder => folder.uri.fsPath));
        } else {
          const buff = Buffer.from(result.testscript_content, 'base64');
          Readable.from(buff).pipe(unzip.Extract({ path: projectPath }));
          vscode.window.showInformationMessage('Soliditai: Tests successfully generated. [FINISHED]');
        }
      } else if (result.status === 'FAILED') {
        vscode.window.showErrorMessage("Generating tests failed with error: " + result.error);
      } else if (result.error !== null) {
        vscode.window.showErrorMessage("Unexpected result of generating tests: " + result.status + ". No tests were generated. " + result.error);
      } else {
        vscode.window.showErrorMessage("Unexpected result of generating tests: " + result.status + ". No tests were generated.");
      }
    };

    function pollResult(httpConfig: any, filename: string) {
      const options = { limit: 180, delay: 10000 };
      const retrier = new Retrier(options);
      retrier
        .resolve(attempt => new Promise((resolve, reject) => {
          const resp = attemptCall(attempt?attempt:0, httpConfig);
          resp.then((response) => {
            if (response.status === 200 && response.data.status !== 'IN_PROGRESS') {
              resolve(response.data);
            } else {
              if (response.status !== 200) {
                reject("Polling gave response status " + response.status + ". Stopped polling. Please try again later.");
              } else {
                reject("Generating tests is in progress for too long. Stopped polling. Please try again later.");
              }
            }
          });
        }))
        .then(
          result => {
            soliditaiCurrentTestGenerationInProgress = '';
            handleResult(result, filename);
          },
          error => {
            soliditaiCurrentTestGenerationInProgress = '';
            vscode.window.showErrorMessage(error);
          }
        );
    }

    let disposable = vscode.commands.registerCommand('soliditai.uploadSolidityContract', async () => {
      const editor = vscode.window.activeTextEditor;
      const filename = vscode.window.activeTextEditor?.document.uri.fsPath ?? '';
      if (editor && fileIsSolidity(filename)) {
        const settings = vscode.workspace.getConfiguration('soliditai');
        if (settings.get('username') === '') {
          vscode.window.showErrorMessage('Enter your soliditai.com username: Press CTRL+, and search for Soliditai');
          return;
        }
        if (settings.get('application-password') === '') {
          vscode.window.showErrorMessage('Enter your soliditai.com application password: Press CTRL+, and search for Soliditai');
          return;
        }
        const filePath = editor.document.uri.fsPath;
        const contents = fs.readFileSync(filePath, {encoding: 'base64'});
        const httpConfig = {
          headers: {
            'Content-Type': 'application/json',
          },
          auth: {
            username: settings.get('username'),
            password: settings.get('application-password')
          },
        };
        uploadAndPoll(httpConfig, filename, contents);
      } else {
        vscode.window.showErrorMessage('Please open a .sol file first.');
      }
    });

    context.subscriptions.push(disposable);
  }