# Soliditai

Our extension generates tests for your Solidity contract. The tests are designed to achieve 100% branch coverage using a proof-of-concept algorithm. While the algorithm is still in development, it can currently handle simple contracts such as [SafeMath.sol](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/0457042d93d9dfd760dbaa06a4d2f1216fdbe297/contracts/utils/math/SafeMath.sol). This allows you to validate your Ethereum contract before deploying it on the blockchain. Once you open your Solidity contract in Visual Studio Code, the extension provides an option to generate tests for your contract. You can initiate the test generation with a click of a button, and the tests will be downloaded and added to your project, making it easy for you to test your contract before deployment. For more information, please visit our website at [soliditai.com](https://soliditai.com)

## Instructions

- Install this extension through the VSCode marketplace.
- Open the settings (CTRL+,) and then under "Extensions" look for "Soliditai". Follow the instructions there to create a free soliditai.com account.
- Copy [SafeMath.sol](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/0457042d93d9dfd760dbaa06a4d2f1216fdbe297/contracts/utils/math/SafeMath.sol) to your project and open it in VS-Code.
- Press CTRL+SHIFT+P and search for Soliditai, to generate the tests.
- The generated tests are placed inside your project. Review them to make sure that your contract works as intended.
- If you want to check back on your previously generated tests, go to [https://soliditai.com/try-it-out/](https://soliditai.com/try-it-out/).

## Caveats

- It will upload the version of your .sol-file that's on disk. In other words, unsaved changes won't be uploaded.
- It will overwrite files that have the exact same name as the generated ones. This is needed because you will want to update your tests as well when you change your contract.